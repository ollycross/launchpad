import Note from '@/Note/Note';
import DrumNoteType from '@/enums/DrumNoteType';

class DrumNote extends Note {
    private readonly name: string;
    private readonly type: DrumNoteType = DrumNoteType.OTHER;

    constructor(
        midiNote: number,
        name: string,
        type: DrumNoteType = DrumNoteType.OTHER,
    ) {
        super(midiNote);

        this.name = name;
        this.type = type;
    }

    public getLabel(): string {
        return this.name;
    }

    public getType(): DrumNoteType {
        return this.type;
    }
}

export default DrumNote;
