import noteIsInMidiRange from '@/utils/noteIsInMidiRange';

class Note {
    private readonly midiNote: number;

    public constructor(midiNote: number) {
        this.midiNote = midiNote;
    }

    public getLabel(): string {
        return '';
    }

    public getMidiNote(): number {
        return this.midiNote;
    }

    isInMidiRange() {
        return noteIsInMidiRange(this.getMidiNote());
    }
}

export default Note;
