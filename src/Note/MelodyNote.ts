import midiToScaleNote from '@/utils/midiToScaleNote.js';
import Note from '@/Note/Note';

class MelodyNote extends Note {
    private readonly degree: number;

    constructor(midiNote: number, degree: number) {
        super(midiNote);
        this.degree = degree;
    }

    isRoot(): boolean {
        return this.degree === 0;
    }

    isFifth(): boolean {
        return this.degree === 4;
    }

    getLabel(): string {
        return midiToScaleNote(this.getMidiNote());
    }
}

export default MelodyNote;
