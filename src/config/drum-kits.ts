import allPurpose from '@/config/drum-kits/all-purpose';
import minimalist from '@/config/drum-kits/minimalist';

export default [allPurpose, minimalist];
