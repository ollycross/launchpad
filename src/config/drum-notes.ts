import DrumNote from '@/Note/DrumNote';
import DrumNoteType from '@/enums/DrumNoteType';

export default {
    ACOUSTIC_BASS_DRUM: new DrumNote(
        35,
        'ACOUSTIC_BASS_DRUM',
        DrumNoteType.KICK,
    ),
    BASS_DRUM_1: new DrumNote(36, 'BASS_DRUM_1', DrumNoteType.KICK),

    ACOUSTIC_SNARE: new DrumNote(38, 'ACOUSTIC_SNARE', DrumNoteType.SNARE),
    ELECTRIC_SNARE: new DrumNote(40, 'ELECTRIC_SNARE', DrumNoteType.SNARE),

    CLOSED_HI_HAT: new DrumNote(42, 'CLOSED_HI_HAT', DrumNoteType.HI_HAT),
    PEDAL_HI_HAT: new DrumNote(44, 'PEDAL_HI_HAT', DrumNoteType.HI_HAT),
    OPEN_HI_HAT: new DrumNote(46, 'OPEN_HI_HAT', DrumNoteType.HI_HAT),

    LOW_FLOOR_TOM: new DrumNote(41, 'LOW_FLOOR_TOM', DrumNoteType.TOM),
    LOW_TOM: new DrumNote(45, 'LOW_TOM', DrumNoteType.TOM),
    LOW_MID_TOM: new DrumNote(47, 'LOW_MID_TOM', DrumNoteType.TOM),
    HI_MID_TOM: new DrumNote(48, 'HI_MID_TOM', DrumNoteType.TOM),
    HIGH_TOM: new DrumNote(50, 'HIGH_TOM', DrumNoteType.TOM),
    HIGH_FLOOR_TOM: new DrumNote(43, 'HIGH_FLOOR_TOM', DrumNoteType.TOM),

    RIDE_CYMBAL_2: new DrumNote(59, 'RIDE_CYMBAL_2', DrumNoteType.CYMBAL),
    CRASH_CYMBAL_1: new DrumNote(49, 'CRASH_CYMBAL_1', DrumNoteType.CYMBAL),
    RIDE_CYMBAL_1: new DrumNote(51, 'RIDE_CYMBAL_1', DrumNoteType.CYMBAL),
    CHINESE_CYMBAL: new DrumNote(52, 'CHINESE_CYMBAL', DrumNoteType.CYMBAL),
    SPLASH_CYMBAL: new DrumNote(55, 'SPLASH_CYMBAL', DrumNoteType.CYMBAL),
    CRASH_CYMBAL_2: new DrumNote(58, 'CRASH_CYMBAL_2', DrumNoteType.CYMBAL),

    COWBELL: new DrumNote(56, 'COWBELL'),
    RIDE_BELL: new DrumNote(53, 'RIDE_BELL'),

    LOW_TIMBALE: new DrumNote(66, 'LOW_TIMBALE'),
    HIGH_TIMBALE: new DrumNote(65, 'HIGH_TIMBALE'),

    LOW_CONGA: new DrumNote(64, 'LOW_CONGA'),
    MUTE_HI_CONGA: new DrumNote(62, 'MUTE_HI_CONGA'),
    OPEN_HI_CONGA: new DrumNote(63, 'OPEN_HI_CONGA'),

    LOW_BONGO: new DrumNote(61, 'LOW_BONGO'),
    HI_BONGO: new DrumNote(60, 'HI_BONGO'),

    LOW_AGOGO: new DrumNote(68, 'LOW_AGOGO'),
    HIGH_AGOGO: new DrumNote(67, 'HIGH_AGOGO'),

    HAND_CLAP: new DrumNote(39, 'HAND_CLAP'),
    SIDE_STICK: new DrumNote(37, 'SIDE_STICK'),
    CABASA: new DrumNote(69, 'CABASA'),
    MARACAS: new DrumNote(70, 'MARACAS'),
    SHORT_WHISTLE: new DrumNote(71, 'SHORT_WHISTLE'),
    LONG_WHISTLE: new DrumNote(72, 'LONG_WHISTLE'),
    SHORT_GUIRO: new DrumNote(73, 'SHORT_GUIRO'),
    LONG_GUIRO: new DrumNote(74, 'LONG_GUIRO'),
    CLAVES: new DrumNote(75, 'CLAVES'),
    HI_WOOD_BLOCK: new DrumNote(76, 'HI_WOOD_BLOCK'),
    LOW_WOOD_BLOCK: new DrumNote(77, 'LOW_WOOD_BLOCK'),
    TAMBOURINE: new DrumNote(54, 'TAMBOURINE'),
    MUTE_CUICA: new DrumNote(78, 'MUTE_CUICA'),
    OPEN_CUICA: new DrumNote(79, 'OPEN_CUICA'),
    MUTE_TRIANGLE: new DrumNote(80, 'MUTE_TRIANGLE'),
    OPEN_TRIANGLE: new DrumNote(57, 'OPEN_TRIANGLE'),
    VIBRASLAP: new DrumNote(81, 'VIBRASLAP'),
};
