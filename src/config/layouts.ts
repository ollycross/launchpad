import Layout from '@/Layout/Layout';
import NotePlayerLayout from '@/Layout/NotePlayerLayout';
import Pad from '@/Entity/Pad';
import DrumKitLayout from '@/Layout/DrumKitLayout';

export default (pad: Pad): Layout[] => {
    return [new NotePlayerLayout(pad), new DrumKitLayout(pad)];
};
