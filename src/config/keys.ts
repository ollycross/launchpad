import Key from '@/Entity/Key';

const keys: Key[] = [
    new Key('c', 60),
    new Key('db', 61, true),
    new Key('d', 62),
    new Key('eb', 63, true),
    new Key('e', 64),
    new Key('f', 65),
    new Key('gb', 66, true),
    new Key('g', 67),
    new Key('ab', 68, true),
    new Key('a', 69),
    new Key('bb', 70, true),
    new Key('b', 71),
];

export default keys;
