import DrumKit from '@/Entity/DrumKit';
import DrumKitItem from '@/Entity/DrumKitItem';
import Coords from '@/Entity/Coords';
import DrumNotes from '@/config/drum-notes';

const name: string = 'Minimalist';

export default new DrumKit(
    [
        // Row 1
        new DrumKitItem(new Coords(3, 7), DrumNotes.BASS_DRUM_1),
        new DrumKitItem(new Coords(4, 7), DrumNotes.BASS_DRUM_1),

        // Row 2
        // Row 3
        new DrumKitItem(new Coords(3, 5), DrumNotes.ACOUSTIC_SNARE),
        new DrumKitItem(new Coords(4, 5), DrumNotes.ACOUSTIC_SNARE),

        // Row 4
        new DrumKitItem(new Coords(1, 4), DrumNotes.CRASH_CYMBAL_1),
        new DrumKitItem(new Coords(2, 4), DrumNotes.CLOSED_HI_HAT),
        new DrumKitItem(new Coords(3, 4), DrumNotes.OPEN_HI_HAT),
        new DrumKitItem(new Coords(4, 4), DrumNotes.OPEN_HI_HAT),
        new DrumKitItem(new Coords(5, 4), DrumNotes.CLOSED_HI_HAT),
        new DrumKitItem(new Coords(6, 4), DrumNotes.SPLASH_CYMBAL),

        // Row 5
        new DrumKitItem(new Coords(2, 3), DrumNotes.RIDE_CYMBAL_1),
        new DrumKitItem(new Coords(3, 3), DrumNotes.LOW_TOM),
        new DrumKitItem(new Coords(4, 3), DrumNotes.LOW_TOM),
        new DrumKitItem(new Coords(5, 3), DrumNotes.RIDE_CYMBAL_1),

        // Row 6
        new DrumKitItem(new Coords(3, 2), DrumNotes.HI_MID_TOM),
        new DrumKitItem(new Coords(4, 2), DrumNotes.LOW_MID_TOM),

        // Row 7
        new DrumKitItem(new Coords(3, 1), DrumNotes.HIGH_TOM),
        new DrumKitItem(new Coords(4, 1), DrumNotes.HIGH_TOM),
    ],
    name,
);
