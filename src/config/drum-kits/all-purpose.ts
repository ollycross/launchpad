import DrumKitItem from '@/Entity/DrumKitItem';
import Coords from '@/Entity/Coords';
import DrumNotes from '@/config/drum-notes';
import DrumKit from '@/Entity/DrumKit';

const name: string = 'All purpose';
export default new DrumKit(
    [
        // Row 1 (Bottom)
        new DrumKitItem(new Coords(0, 7), DrumNotes.CRASH_CYMBAL_1),
        new DrumKitItem(new Coords(1, 7), DrumNotes.ACOUSTIC_SNARE),
        new DrumKitItem(new Coords(2, 7), DrumNotes.ACOUSTIC_BASS_DRUM),
        new DrumKitItem(new Coords(3, 7), DrumNotes.ACOUSTIC_BASS_DRUM),
        new DrumKitItem(new Coords(4, 7), DrumNotes.ACOUSTIC_BASS_DRUM),
        new DrumKitItem(new Coords(5, 7), DrumNotes.ACOUSTIC_BASS_DRUM),
        new DrumKitItem(new Coords(6, 7), DrumNotes.ACOUSTIC_SNARE),
        new DrumKitItem(new Coords(7, 7), DrumNotes.CRASH_CYMBAL_2),

        // Row 2
        new DrumKitItem(new Coords(3, 6), DrumNotes.ELECTRIC_SNARE),
        new DrumKitItem(new Coords(4, 6), DrumNotes.ELECTRIC_SNARE),

        // Row 3
        new DrumKitItem(new Coords(1, 5), DrumNotes.SIDE_STICK),
        new DrumKitItem(new Coords(3, 5), DrumNotes.ACOUSTIC_SNARE),
        new DrumKitItem(new Coords(4, 5), DrumNotes.ACOUSTIC_SNARE),
        new DrumKitItem(new Coords(6, 5), DrumNotes.SIDE_STICK),

        // Row 4
        new DrumKitItem(new Coords(1, 4), DrumNotes.SPLASH_CYMBAL),
        new DrumKitItem(new Coords(2, 4), DrumNotes.CLOSED_HI_HAT),
        new DrumKitItem(new Coords(3, 4), DrumNotes.OPEN_HI_HAT),
        new DrumKitItem(new Coords(4, 4), DrumNotes.OPEN_HI_HAT),
        new DrumKitItem(new Coords(5, 4), DrumNotes.CLOSED_HI_HAT),
        new DrumKitItem(new Coords(6, 4), DrumNotes.CHINESE_CYMBAL),

        // Row 5
        new DrumKitItem(new Coords(2, 3), DrumNotes.RIDE_CYMBAL_1),
        new DrumKitItem(new Coords(3, 3), DrumNotes.RIDE_BELL),
        new DrumKitItem(new Coords(4, 3), DrumNotes.RIDE_BELL),
        new DrumKitItem(new Coords(5, 3), DrumNotes.RIDE_CYMBAL_2),

        // Row 6
        new DrumKitItem(new Coords(1, 2), DrumNotes.LOW_FLOOR_TOM),
        new DrumKitItem(new Coords(2, 2), DrumNotes.LOW_TOM),
        new DrumKitItem(new Coords(3, 2), DrumNotes.LOW_MID_TOM),
        new DrumKitItem(new Coords(4, 2), DrumNotes.HI_MID_TOM),
        new DrumKitItem(new Coords(5, 2), DrumNotes.HIGH_TOM),
        new DrumKitItem(new Coords(6, 2), DrumNotes.HIGH_FLOOR_TOM),
    ],
    name,
);
