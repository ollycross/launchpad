import Scale from '@/Entity/Scale';

const scales: Scale[] = [
    new Scale('ionian', [0, 2, 4, 5, 7, 9, 11]),
    new Scale('dorian', [0, 2, 3, 5, 7, 9, 10]),
    new Scale('phyrigian', [0, 1, 3, 5, 7, 9, 10]),
    new Scale('lydian', [0, 2, 4, 6, 7, 9, 11]),
    new Scale('mixolydian', [0, 2, 4, 5, 7, 9, 10]),
    new Scale('aeolian', [0, 2, 3, 5, 7, 8, 10]),
    new Scale('locrian', [0, 1, 3, 5, 6, 9, 10]),
];

export default scales;
