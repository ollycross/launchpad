import Layout from '@/Layout/Layout';

class LayoutCollection {
    private readonly items: Layout[] = [];
    constructor(items: any[] = []) {
        this.items = items;
    }

    public get(index: number): Layout | null {
        return this.items[index] || null;
    }

    public has(ix: number): boolean {
        return typeof this.items[ix] !== 'undefined';
    }
}

export default LayoutCollection;
