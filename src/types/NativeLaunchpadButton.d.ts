type NativeLaunchpadButton = {
    id: symbol;
} & [number, number];

export default NativeLaunchpadButton;
