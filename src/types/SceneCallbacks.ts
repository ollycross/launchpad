declare type SceneCallbacksOutput = [
    () => Promise<boolean>,
    () => Promise<boolean>,
];

declare type SceneCallbacksInput =
    | (() => Promise<boolean>)
    | SceneCallbacksOutput
    | undefined[];
