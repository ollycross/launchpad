declare type NotePlayerSettingsConfig = {
    grid: Grid;
    keyRoot: number;
    scale: number;
    octave: number;
    noteOffset: number;
};
