type LaunchpadPressEvent = {
    id: symbol;
    x: number;
    y: number;
    pressed: boolean;
    0: number;
    1: number;
    length: 2;
};

export default LaunchpadPressEvent;
