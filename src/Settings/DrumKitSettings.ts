import Settings from '@/Settings/Settings';
import DrumKit from '@/Entity/DrumKit';
import drumKits from '@/config/drum-kits';

class DrumKitSettings extends Settings {
    protected config: DrumKitSettingsConfig;

    constructor() {
        super();

        this.config = {
            kit: 0,
        };
    }

    public setKit(kit: number): void {
        this.set('kit', kit);

        const _kit: DrumKit = this.getKit();
        console.log(`Switched to kit ${_kit.getName() || kit}`);
    }

    public getKit(): DrumKit {
        return drumKits[this.get('kit')];
    }
}

export default DrumKitSettings;
