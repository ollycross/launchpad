class Settings {
    protected config: {};
    constructor() {
        this.get = this.get.bind(this);
        this.set = this.set.bind(this);
    }

    get(key: string): any {
        return typeof this.config[key] === 'undefined'
            ? null
            : this.config[key];
    }

    set(key: string, value: any): void {
        this.config[key] = value;
    }
}

export default Settings;
