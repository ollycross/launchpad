import scales from '@/config/scales';
import keys from '@/config/keys';
import Settings from '@/Settings/Settings';
import Scale from '@/Entity/Scale';
import Key from '@/Entity/Key';

class NotePlayerSettings extends Settings {
    protected config: NotePlayerSettingsConfig;

    constructor() {
        super();

        this.config = {
            grid: {
                height: 8,
                width: 8,
            },
            keyRoot: this.rootFromKey('c'),
            scale: this.scaleFromName('ionian'),
            octave: -2,
            noteOffset: 3,
        };

        this.octaveUp = this.octaveUp.bind(this);
        this.octaveDown = this.octaveDown.bind(this);
        this.noteOffsetUp = this.noteOffsetUp.bind(this);
        this.noteOffsetDown = this.noteOffsetDown.bind(this);
    }

    public octaveUp(): void {
        this.setOctave(this.get('octave') + 1);
    }

    public octaveDown(): void {
        this.setOctave(this.get('octave') - 1);
    }

    private setOctave(octave: number): void {
        this.set('octave', octave);
        console.log(`Set octave to ${this.getOctave()}`);
    }

    getOctave(): number {
        return this.get('octave');
    }

    public noteOffsetUp(): void {
        this.setNoteOffset(this.getNoteOffset() + 1);
    }

    public noteOffsetDown(): void {
        this.setNoteOffset(this.getNoteOffset() - 1);
    }

    public getNoteOffset(): number {
        return this.get('noteOffset');
    }

    private setNoteOffset(noteOffset: number): void {
        this.set('noteOffset', noteOffset);
        console.log(`Set noteOffset to ${this.getNoteOffset()}`);
    }

    public getScale(): Scale {
        return scales[this.get('scale')];
    }

    public scaleUp(): void {
        const scale = this.get('scale');

        this.setScale(scale === scales.length - 1 ? 0 : scale + 1);
    }

    public scaleDown(): void {
        const scale = this.get('scale');

        this.setScale(scale - 1 < 0 ? scales.length - 1 : scale - 1);
    }

    private setScale(scale: number): void {
        this.set('scale', scale);

        console.log(`Changed scale to new scale: ${this.getScale().name}`);
    }

    public keyUp(): void {
        const key = this.get('keyRoot');

        this.setKey(key === keys.length - 1 ? 0 : key + 1);
    }

    public keyDown(): void {
        const key = this.get('keyRoot');

        this.setKey(key - 1 < 0 ? keys.length - 1 : key - 1);
    }

    public getKey(): Key {
        return keys[this.get('keyRoot')];
    }

    public setKey(key: number): void {
        this.set('keyRoot', key);

        const _key: Key = keys[key];

        let { note } = _key;

        note = `${note.slice(0, 1).toUpperCase()}${note.slice(1).toLowerCase()}`;

        console.log(`Set key to: ${note}`);
    }

    public scaleFromName(name: string): number {
        return (
            scales.findIndex((scale: Scale): boolean => scale.name === name) ||
            0
        );
    }

    rootFromKey(keyName: string): number {
        return keys.findIndex((key: Key): boolean => key.note === keyName) || 0;
    }

    public getRoot(): number {
        return this.getKey().root;
    }

    getGrid(): Grid {
        return this.get('grid');
    }
}

export default NotePlayerSettings;
