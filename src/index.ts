#!/usr/bin/env node

import LaunchpadMini from 'launchpad-mini';
import Pad from '@/Entity/Pad.js';
import Launchpad from 'launchpad-mini';

(async (): Promise<void> => {
    const launchpadMini: Launchpad = new LaunchpadMini();
    await launchpadMini.connect();

    const pad: Pad = new Pad(launchpadMini);

    await pad.init();
})();
