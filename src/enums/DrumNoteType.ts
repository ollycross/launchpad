enum DrumNoteType {
    OTHER,
    KICK,
    SNARE,
    HI_HAT,
    TOM,
    CYMBAL,
}

export default DrumNoteType;
