import { Color } from 'launchpad-mini/types/lib/colors';
import Layout from '@/Layout/Layout';
import Coords from '@/Entity/Coords';

class Button {
    private readonly coords: Coords;
    protected readonly layout: Layout;
    protected lastColor: Color | null;
    protected color: Color | null;

    constructor(coords: Coords, layout: Layout) {
        this.coords = coords;
        this.layout = layout;
        this.lastColor = null;
        this.color = null;
    }

    public getCoords(): Coords {
        return this.coords;
    }

    public async setColor(color: Color): Promise<void> {
        this.lastColor = this.color;
        this.color = color;
        await this.layout
            .getPad()
            .setButtonColor(color, [this.getCoords().toArray()]);
    }

    public onKeyDown(): Promise<boolean> {
        return Promise.resolve(false);
    }

    public onKeyUp(): Promise<boolean> {
        return Promise.resolve(false);
    }
}

export default Button;
