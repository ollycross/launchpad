import Button from '@/Button/Button.js';
import Layout from '@/Layout/Layout';
import Coords from '@/Entity/Coords';

class CallbackButton extends Button {
    constructor(
        coords: Coords,
        layout: Layout,
        onKeyDownCallback: () => Promise<boolean>,
        onKeyUpCallback: () => Promise<boolean>,
    ) {
        super(coords, layout);

        this.onKeyDown = onKeyDownCallback;
        this.onKeyUp = onKeyUpCallback;
    }
}

export default CallbackButton;
