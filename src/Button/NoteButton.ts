import LaunchpadMini from 'launchpad-mini';
import Button from '@/Button/Button.js';
import Note from '@/Note/Note';
import Coords from '@/Entity/Coords';
import Layout from '@/Layout/Layout';

const NOTE_ON: number = 144;
const NOTE_OFF: number = 128;
const DEFAULT_VELOCITY: number = 80;

class NoteButton extends Button {
    private readonly note: Note;
    protected layout: Layout;

    constructor(coords: Coords, layout: Layout, note: Note) {
        super(coords, layout);

        this.note = note;
    }

    public async onKeyDown(): Promise<boolean> {
        await this.setColor(LaunchpadMini.Colors.yellow);

        await this.startNote();

        return false;
    }

    public async onKeyUp(): Promise<boolean> {
        await this.setColor(this.lastColor);

        await this.stopNote();

        return false;
    }

    public getNote(): Note {
        return this.note;
    }

    private async startNote(channel: number = 0): Promise<void> {
        if (this.getNote()) {
            let note: string = this.note.getLabel();
            console.log(`${note} (${this.note.getMidiNote()})`.trim());
            return this._sendNote(NOTE_ON, channel);
        }
    }

    private async stopNote(channel: number = 0): Promise<void> {
        if (this.getNote()) {
            return this._sendNote(NOTE_OFF, channel, 0);
        }
    }

    private async _sendNote(
        startStop: number,
        channel: number = 0,
        velocity: number = DEFAULT_VELOCITY,
    ): Promise<void> {
        const code: number = startStop + channel;
        const data: [number, number, number] = [
            code,
            this.note.getMidiNote(),
            velocity,
        ];

        this.layout.getOutput().sendMessage(data);

        return Promise.resolve();
    }
}

export default NoteButton;
