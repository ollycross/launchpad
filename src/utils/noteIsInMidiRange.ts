export default function noteIsInMidiRange(note: number): boolean {
    const range: number[] = [21, 127];

    return note >= range[0] && note <= range[1];
}
