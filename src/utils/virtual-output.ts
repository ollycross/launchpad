import { Output } from '@julusian/midi';

const makeVirtualOutput = (name: string = 'launchpad-mini-node'): Output => {
    // Set up a new output.
    const output: Output = new Output();

    // Count the available output ports.
    output.openVirtualPort(name);

    return output;
};

export default makeVirtualOutput;
