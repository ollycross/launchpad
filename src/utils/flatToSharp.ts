export default function flatToSharp(flat: string): string {
    const _flat: string = flat.replace(/[0-9]/g, '').toUpperCase();

    return (
        {
            AB: 'G#',
            BB: 'A#',
            DB: 'C#',
            EB: 'D#',
            GB: 'F#',
        }[_flat] || flat
    );
}
