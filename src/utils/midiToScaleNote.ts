export default function midiToScaleNote(note: number): string {
    const map: string[] = [
        'A',
        'Bb',
        'B',
        'C',
        'Db',
        'D',
        'Eb',
        'E',
        'F',
        'Gb',
        'G',
        'Ab',
    ];

    const octave: number = Math.ceil((note - 21) / 12);
    const normalised: number = (note - 21) % 12;

    let scaleNote: string = map[normalised];

    scaleNote = `${scaleNote.slice(0, 1).toUpperCase()}${scaleNote.slice(1).toLowerCase()}`;

    return `${scaleNote}${octave}`;
}
