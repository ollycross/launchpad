class Key {
    public note: string;
    public root: number;
    public flat: boolean;

    constructor(note: string, root: number, flat: boolean = false) {
        this.note = note;
        this.root = root;
        this.flat = flat;
    }
}

export default Key;
