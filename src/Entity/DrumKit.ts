import DrumKitItem from '@/Entity/DrumKitItem';
import Coords from '@/Entity/Coords';

class DrumKit {
    private readonly items: DrumKitItem[];
    private readonly name: string = '';

    constructor(items: DrumKitItem[], name: string = '') {
        this.items = items;
        this.name = name;
    }

    public forCoords(coords: Coords): DrumKitItem | null {
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].isCoords(coords)) {
                return this.items[i];
            }
        }

        return null;
    }

    public getName(): string {
        return this.name;
    }
}

export default DrumKit;
