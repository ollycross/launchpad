class Coords {
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    public toArray(): [number, number] {
        return [this.x, this.y];
    }

    public equals(coords: Coords): boolean {
        return this.x === coords.x && this.y === coords.y;
    }
}

export default Coords;
