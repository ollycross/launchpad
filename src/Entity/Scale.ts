class Scale {
    public name: string;
    public intervals: number[];

    constructor(name: string, intervals: number[]) {
        this.name = name;
        this.intervals = intervals;
    }
}

export default Scale;
