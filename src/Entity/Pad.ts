import LaunchpadMini from 'launchpad-mini';
import { Color } from 'launchpad-mini/types/lib/colors';
import Layout from '@/Layout/Layout';
import LaunchpadPressEvent from '@/types/LaunchpadPressEvent';
import LayoutCollection from '@/Collection/LayoutCollection.js';
import makeVirtualOutput from '@/utils/virtual-output';
import { Output } from '@julusian/midi';
import layouts from '@/config/layouts';

class Pad {
    private readonly launchpad: LaunchpadMini;
    private readonly layoutCollection: LayoutCollection;
    private currentLayoutIx: number = -1;
    private currentLayout: Layout;
    private readonly output: Output;

    constructor(launchpad: LaunchpadMini) {
        this.launchpad = launchpad;
        this.output = makeVirtualOutput();
        this.layoutCollection = new LayoutCollection(layouts(this));

        this.handleKey = this.handleKey.bind(this);
    }

    public async init(): Promise<void> {
        this.launchpad.on('key', this.handleKey);

        await this.setLayout(0);
    }

    private async setLayout(index: number): Promise<void> {
        if (this.currentLayout) {
            await this.currentLayout.close();
        }

        if (this.currentLayoutIx !== index) {
            if (!this.layoutCollection.has(index)) {
                console.error(`No layout found for index ${index}`);
                return;
            }

            this.currentLayoutIx = index;
            this.currentLayout = this.layoutCollection.get(index);

            this.reset();
            await this.setLayoutFunctionLight(index);
            await this.currentLayout.init();
        }
    }

    public async setLayoutFunctionLight(index: number): Promise<void> {
        await this.setButtonColor(
            LaunchpadMini.Colors.off,
            LaunchpadMini.Buttons.Automap,
        );

        await this.setButtonColor(LaunchpadMini.Colors.amber, [[index, 8]]);

        return;
    }

    private async handleKey(event: LaunchpadPressEvent): Promise<void> {
        const { pressed } = event;
        const method: (event: LaunchpadPressEvent) => Promise<boolean> = pressed
            ? this.handleKeyDown
            : this.handleKeyUp;
        const shouldRefresh = await method.call(this, event);

        if (shouldRefresh) {
            await this.currentLayout.refresh();
        }
    }

    private isFunctionKey = ({ y }: LaunchpadPressEvent): boolean => {
        return y === 8;
    };

    private async handleKeyDown(event: LaunchpadPressEvent): Promise<boolean> {
        if (this.isFunctionKey(event)) {
            return this.handleFunctionKeyDown(event);
        }

        const shouldRefresh: boolean =
            await this.currentLayout.handleKeyDown(event);
        if (shouldRefresh) {
            await this.currentLayout.refresh();
        }
    }

    private async handleKeyUp(event: LaunchpadPressEvent): Promise<boolean> {
        if (this.isFunctionKey(event)) {
            return this.handleFunctionKeyUp();
        }

        return this.currentLayout.handleKeyUp(event);
    }

    private async handleFunctionKeyDown({
        x,
    }: LaunchpadPressEvent): Promise<boolean> {
        const index: number = this.layoutCollection.has(x) ? x : 0;

        await this.setLayout(index);

        return true;
    }

    private handleFunctionKeyUp(): Promise<boolean> {
        return Promise.resolve(false);
    }

    private reset(): void {
        this.launchpad.reset();
    }

    public async setButtonColor(
        color: Color,
        buttons: number[] | number[][],
    ): Promise<any> {
        return this.launchpad.col(color, buttons);
    }

    public getOutput(): Output {
        return this.output;
    }
}

export default Pad;
