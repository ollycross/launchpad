import Coords from '@/Entity/Coords';
import DrumNote from '@/Note/DrumNote';

class DrumKitItem {
    private readonly _coords: Coords;
    private readonly _note: DrumNote;

    constructor(coords: Coords, note: DrumNote) {
        this._coords = coords;
        this._note = note;
    }

    getNote(): DrumNote {
        return this._note;
    }

    getCoords(): Coords {
        return this._coords;
    }

    isCoords(coords: Coords): boolean {
        return this.getCoords().equals(coords);
    }
}

export default DrumKitItem;
