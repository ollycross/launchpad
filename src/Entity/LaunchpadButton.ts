import NativeLaunchpadButton from '@/types/NativeLaunchpadButton';
import Coords from '@/Entity/Coords';

class LaunchpadButton {
    public id: symbol;
    public coords: Coords;

    constructor(id: symbol, x: number, y: number) {
        this.id = id;
        this.coords = new Coords(x, y);
    }

    /** Native library incorrectly defines buttons as numbers */
    public static fromNativeButton(nativeButton: number): LaunchpadButton {
        const _button: NativeLaunchpadButton = <NativeLaunchpadButton>(
            (<unknown>nativeButton)
        );

        const { id } = _button;
        const [x, y] = _button;

        return new LaunchpadButton(id, x, y);
    }
}

export default LaunchpadButton;
