import LaunchpadMini, { Colors } from 'launchpad-mini';
import Button from '@/Button/Button.js';
import CallbackButton from '@/Button/CallbackButton.js';
import Settings from '@/Settings/Settings.js';
import Pad from '@/Entity/Pad';
import LaunchpadPressEvent from '@/types/LaunchpadPressEvent';
import { Output } from '@julusian/midi';
import LaunchpadButton from '@/Entity/LaunchpadButton';

const boundResolve = Promise.resolve.bind(Promise);

class Layout {
    protected readonly pad: Pad;
    private readonly buttons: Record<symbol, Button>;
    protected config: Settings;

    constructor(pad: Pad) {
        this.pad = pad;
        this.buttons = {};
        this.config = new Settings();
    }

    protected getConfig(): Settings {
        return this.config;
    }

    public getPad(): Pad {
        return this.pad;
    }

    public getOutput(): Output {
        return this.getPad().getOutput();
    }

    private mapGrid(): void {
        LaunchpadMini.Buttons.Grid.forEach((button: number): void => {
            const _button: LaunchpadButton =
                LaunchpadButton.fromNativeButton(button);
            this.buttons[_button.id] = this.mapGridButton(_button);
        });
    }

    mapScene(): void {
        LaunchpadMini.Buttons.Scene.forEach((button: number): void => {
            const _button: LaunchpadButton =
                LaunchpadButton.fromNativeButton(button);
            this.buttons[_button.id] = this.mapSceneButton(_button);
        });
    }

    mapGridButton(button: LaunchpadButton): Button {
        const { coords } = button;

        return new Button(coords, this);
    }

    mapSceneButton(button: LaunchpadButton): CallbackButton {
        const { coords } = button;

        const [sceneCallbackDown, sceneCallbackUp]: SceneCallbacksOutput =
            this.getSceneCallback(coords.y);

        return new CallbackButton(
            coords,
            this,
            sceneCallbackDown,
            sceneCallbackUp,
        );
    }

    async init(): Promise<void> {
        return this.initBoard();
    }

    async refresh(): Promise<void> {
        return this.initBoard();
    }

    async close(): Promise<void> {
        return Promise.resolve();
    }

    async initBoard(): Promise<void> {
        await this.pad.setButtonColor(Colors.off, LaunchpadMini.Buttons.Grid);

        this.mapGrid();
        this.mapScene();

        for (const button of LaunchpadMini.Buttons.Scene) {
            const _button: LaunchpadButton =
                LaunchpadButton.fromNativeButton(button);
            const { id, coords } = _button;

            if (this.hasSceneCallback(coords.y)) {
                await this.getButton(id).setColor(LaunchpadMini.Colors.amber);
            }
        }

        return Promise.resolve();
    }

    async handleKeyUp(event: LaunchpadPressEvent): Promise<boolean> {
        const button: Button = this.getButton(event.id);
        if (button) {
            return button.onKeyUp();
        }

        return false;
    }

    async handleKeyDown(event: LaunchpadPressEvent): Promise<boolean> {
        const button: Button = this.getButton(event.id);
        if (button) {
            return button.onKeyDown();
        }

        return false;
    }

    getButton(id: symbol): Button {
        return this.buttons[id];
    }

    hasSceneCallback(y: number): boolean {
        const [sceneCallbackDown, sceneCallbackUp] = this.getSceneCallback(y);

        return (
            sceneCallbackDown !== boundResolve ||
            sceneCallbackUp !== boundResolve
        );
    }

    getSceneCallbacks(): Record<number, SceneCallbacksInput> {
        return {};
    }

    isSceneCallbacksOutput(
        callback: SceneCallbacksInput | SceneCallbacksOutput,
    ): callback is SceneCallbacksOutput {
        return (
            Array.isArray(callback) &&
            callback.length === 2 &&
            callback.every((item) => typeof item === 'function')
        );
    }

    isSceneCallbackFunction(
        callback: SceneCallbacksInput,
    ): callback is () => Promise<boolean> {
        return typeof callback === 'function';
    }

    ensureCallbackArray(callback: SceneCallbacksInput): SceneCallbacksOutput {
        if (this.isSceneCallbacksOutput(callback)) {
            return callback;
        }

        if (this.isSceneCallbackFunction(callback)) {
            return [callback, boundResolve];
        }

        return [boundResolve, boundResolve];
    }

    getSceneCallback(y: number): SceneCallbacksOutput {
        const sceneCallbacks: Record<number, SceneCallbacksInput> =
            this.getSceneCallbacks();

        return this.ensureCallbackArray(sceneCallbacks[y] || []);
    }
}

export default Layout;
