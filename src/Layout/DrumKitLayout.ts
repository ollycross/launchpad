import LaunchpadMini, { Colors } from 'launchpad-mini';
import { Color } from 'launchpad-mini/types/lib/colors';
import Layout from '@/Layout/Layout.js';
import NoteButton from '@/Button/NoteButton.js';
import Pad from '@/Entity/Pad';
import DrumKitSettings from '@/Settings/DrumKitSettings';
import DrumNote from '@/Note/DrumNote';
import DrumKit from '@/Entity/DrumKit';
import LaunchpadButton from '@/Entity/LaunchpadButton';
import DrumKitItem from '@/Entity/DrumKitItem';
import DrumNoteType from '@/enums/DrumNoteType';
import drumKits from '@/config/drum-kits';

class DrumKitLayout extends Layout {
    protected config: DrumKitSettings;

    constructor(pad: Pad) {
        super(pad);
        this.config = new DrumKitSettings();
    }

    protected getConfig(): DrumKitSettings {
        return this.config;
    }

    getSceneCallbacks(): Record<number, SceneCallbacksInput> {
        const callbacks: Record<number, SceneCallbacksInput> = {};
        for (let i = 0; i < drumKits.length; i++) {
            if (i > 7) {
                continue;
            }

            callbacks[i] = async (): Promise<boolean> => {
                this.getConfig().setKit(i);

                return true;
            };
        }

        return {
            ...super.getSceneCallbacks(),
            ...callbacks,
        };
    }

    async initBoard(): Promise<void> {
        await super.initBoard();

        for (const launchpadButton of LaunchpadMini.Buttons.Grid) {
            const { id }: LaunchpadButton =
                LaunchpadButton.fromNativeButton(launchpadButton);

            const button: NoteButton = <NoteButton>this.getButton(id);

            if (button !== null) {
                const color: Color = this.noteColor(<DrumNote>button.getNote());

                await button.setColor(color);
            }
        }

        return Promise.resolve();
    }

    noteColor(note: DrumNote): Color {
        switch (note.getType()) {
            case DrumNoteType.KICK:
                return Colors.red;
            case DrumNoteType.SNARE:
                return Colors.green;
            case DrumNoteType.HI_HAT:
                return Colors.yellow;
            default:
                return Colors.amber;
        }
    }

    mapGridButton(button: LaunchpadButton): NoteButton | null {
        const { coords } = button;

        const kit: DrumKit = this.getConfig().getKit();
        const drumKitItem: DrumKitItem | null = kit.forCoords(coords);

        if (drumKitItem !== null) {
            const note: DrumNote = drumKitItem.getNote();

            return new NoteButton(coords, this, note);
        }

        return null;
    }
}

export default DrumKitLayout;
