import LaunchpadMini from 'launchpad-mini';
import { Color } from 'launchpad-mini/types/lib/colors';
import Layout from '@/Layout/Layout.js';
import NoteButton from '@/Button/NoteButton.js';
import NotePlayerSettings from '@/Settings/NotePlayerSettings.js';
import Pad from '@/Entity/Pad';
import Scale from '@/Entity/Scale';
import scales from '@/config/scales';
import MelodyNote from '@/Note/MelodyNote.js';
import LaunchpadButton from '@/Entity/LaunchpadButton';

class NotePlayerLayout extends Layout {
    protected config: NotePlayerSettings;
    private adjustedScale: number[] = [];

    constructor(pad: Pad) {
        super(pad);
        this.config = new NotePlayerSettings();
    }

    protected getConfig(): NotePlayerSettings {
        return this.config;
    }

    getSceneCallbacks(): Record<number, SceneCallbacksInput> {
        return {
            ...super.getSceneCallbacks(),
            ...{
                0: async (): Promise<boolean> => {
                    this.getConfig().octaveUp();
                    return true;
                },
                1: async (): Promise<boolean> => {
                    this.getConfig().octaveDown();
                    return true;
                },
                2: async (): Promise<boolean> => {
                    this.getConfig().scaleDown();
                    return true;
                },
                3: async (): Promise<boolean> => {
                    this.getConfig().scaleUp();
                    return true;
                },
                4: async (): Promise<boolean> => {
                    this.getConfig().keyDown();
                    return true;
                },
                5: async (): Promise<boolean> => {
                    this.getConfig().keyUp();
                    return true;
                },
                6: async (): Promise<boolean> => {
                    this.getConfig().noteOffsetUp();
                    return true;
                },
                7: async (): Promise<boolean> => {
                    this.getConfig().noteOffsetDown();
                    return true;
                },
            },
        };
    }

    getKeyNotes(): number[] {
        const root: number = this.getConfig().getRoot();
        const octave: number = this.getConfig().getOctave();
        const scale: number[] = this.getConfig().getScale().intervals;

        const startingNote: number = root + octave * 12;

        return scale.map((semitones: number) => startingNote + semitones);
    }

    async initBoard(): Promise<void> {
        this.adjustedScale = this.getKeyNotes();
        await super.initBoard();

        for (const launchpadButton of LaunchpadMini.Buttons.Grid) {
            const { id }: LaunchpadButton =
                LaunchpadButton.fromNativeButton(launchpadButton);

            const button: NoteButton = <NoteButton>this.getButton(id);

            if (button !== null) {
                const note: MelodyNote = <MelodyNote>button.getNote();
                const color: Color = this.noteColor(note);
                await button.setColor(color);
            }
        }

        for (const button of LaunchpadMini.Buttons.Scene) {
            const _button: LaunchpadButton =
                LaunchpadButton.fromNativeButton(button);
            const { id, coords } = _button;

            const { note, flat = false } = this.config.getKey();

            const keyPos: number = ['A', 'B', 'C', 'D', 'E', 'F', 'G'].indexOf(
                note.slice(0, 1).toUpperCase(),
            );

            if (flat && coords.y === 7) {
                await this.getButton(id)?.setColor(LaunchpadMini.Colors.red);
            } else if (coords.y === keyPos) {
                await this.getButton(id)?.setColor(LaunchpadMini.Colors.red);
            }
        }

        await this.pad.setButtonColor(LaunchpadMini.Colors.off, [
            [1, 8],
            [2, 8],
            [3, 8],
            [4, 8],
            [5, 8],
            [6, 8],
            [7, 8],
        ]);

        const scale: Scale = this.getConfig().getScale();
        const scaleIx: number = scales.findIndex(
            (_scale: Scale): boolean => _scale.name === scale.name,
        );

        if (scaleIx !== -1) {
            await this.pad.setButtonColor(LaunchpadMini.Colors.red, [
                [scaleIx + 1, 8],
            ]);
        }

        return Promise.resolve();
    }

    noteColor(note: MelodyNote): Color {
        if (note.isRoot()) {
            return LaunchpadMini.Colors.green;
        }

        if (note.isFifth()) {
            return LaunchpadMini.Colors.red;
        }

        return LaunchpadMini.Colors.amber;
    }

    getNote({ coords }: LaunchpadButton): MelodyNote | null {
        const { height, width } = this.getConfig().getGrid();
        const noteOffset: number = this.getConfig().getNoteOffset();

        const { adjustedScale } = this;
        const index: number =
            width * height - width - height * coords.y + coords.x - noteOffset;
        const octave: number = Math.floor(index / adjustedScale.length);
        const degree: number = index - adjustedScale.length * octave;

        const midiNote: number = adjustedScale[degree] + octave * 12;

        try {
            return new MelodyNote(midiNote, degree);
        } catch (e) {
            return null;
        }
    }

    mapGridButton(button: LaunchpadButton): NoteButton | null {
        const { coords } = button;
        const note: MelodyNote = this.getNote(button);

        if (note.isInMidiRange()) {
            return new NoteButton(coords, this, note);
        }

        return null;
    }
}

export default NotePlayerLayout;
